# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\trackerUi.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(628, 426)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gbInput = QtWidgets.QGroupBox(self.centralwidget)
        self.gbInput.setObjectName("gbInput")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.gbInput)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leInputVideo = QtWidgets.QLineEdit(self.gbInput)
        self.leInputVideo.setObjectName("leInputVideo")
        self.horizontalLayout.addWidget(self.leInputVideo)
        self.btnInputVideoBrowser = QtWidgets.QPushButton(self.gbInput)
        self.btnInputVideoBrowser.setObjectName("btnInputVideoBrowser")
        self.horizontalLayout.addWidget(self.btnInputVideoBrowser)
        self.verticalLayout_2.addWidget(self.gbInput)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.gbSaveDataTo = QtWidgets.QGroupBox(self.centralwidget)
        self.gbSaveDataTo.setObjectName("gbSaveDataTo")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.gbSaveDataTo)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.leExportLocation = QtWidgets.QLineEdit(self.gbSaveDataTo)
        self.leExportLocation.setObjectName("leExportLocation")
        self.horizontalLayout_2.addWidget(self.leExportLocation)
        self.btnExportBrowser = QtWidgets.QPushButton(self.gbSaveDataTo)
        self.btnExportBrowser.setObjectName("btnExportBrowser")
        self.horizontalLayout_2.addWidget(self.btnExportBrowser)
        self.verticalLayout_2.addWidget(self.gbSaveDataTo)
        self.btnStartProcessing = QtWidgets.QPushButton(self.centralwidget)
        self.btnStartProcessing.setObjectName("btnStartProcessing")
        self.verticalLayout_2.addWidget(self.btnStartProcessing)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 628, 21))
        self.menubar.setObjectName("menubar")
        self.menuExit = QtWidgets.QMenu(self.menubar)
        self.menuExit.setObjectName("menuExit")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuExit.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.gbInput.setTitle(_translate("MainWindow", "Input Video"))
        self.btnInputVideoBrowser.setText(_translate("MainWindow", "Browse"))
        self.gbSaveDataTo.setTitle(_translate("MainWindow", "Save Data to"))
        self.btnExportBrowser.setText(_translate("MainWindow", "Browse"))
        self.btnStartProcessing.setText(_translate("MainWindow", "Start Processing"))
        self.menuExit.setTitle(_translate("MainWindow", "Exit"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

