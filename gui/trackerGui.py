
from PyQt5.QtWidgets import QFileDialog
from gui.trackerUi import Ui_MainWindow
from solver.faceTracker import FaceLandmarkSolver

class TrackerGui(Ui_MainWindow):
	"""docstring for TrackerGui"""
	def __init__(self):
		super(TrackerGui, self).__init__()
		self._isProcessing = False

	def initializeUi(self, parentWindow):
		self.setupUi(parentWindow)
		self._solver = None
		#Just to test
		self.leInputVideo.setText("")
		self.leExportLocation.setText("")
		#Set Signals
		self.btnInputVideoBrowser.clicked.connect(self.browseVideoFile)
		self.btnExportBrowser.clicked.connect(self.saveFileLocation)
		self.btnStartProcessing.clicked.connect(self.startOrStopProcess)

	def browseVideoFile(self):
		options = QFileDialog.Options()
		fileName, _ = QFileDialog.getOpenFileName(None,
			"QFileDialog.getOpenFileName()", 
			"",
			"All Files (*);;MPEG4 Files (*.mp4)", 
			options=options
		)
		self.leInputVideo.setText(fileName)
		self.statusbar.showMessage("Selected {}".format(fileName))

	def saveFileLocation(self):
		options = QFileDialog.Options()
		fileName, _ = QFileDialog.getSaveFileName(None,
			"QFileDialog.getOpenFileName()", 
			"",
			"json Files (*.json)", 
			options=options
		)
		self.leExportLocation.setText(fileName)
		self.statusbar.showMessage("Opted to save @ {}".format(fileName))

	def startOrStopProcess(self):
		if self._isProcessing:
			self.stopProcessing()
		else:
			self.startProcessing()

	def startProcessing(self):
		videoPath = self.leInputVideo.text()
		exportPath = self.leExportLocation.text()

		if not videoPath:
			print("Select input video first")
			return

		if not exportPath:
			print("Specify where to save data")
			return 

		self.btnStartProcessing.setText("Stop Processing")
		self.enableAllFields(False)
		self._isProcessing = True
		self._solver = FaceLandmarkSolver(
			str(videoPath),
			"solver/shape_predictor_68_face_landmarks.dat"
			)
		exitCode = self._solver.startSolving()
		if exitCode == 0:
			#Successfully Processed
			import json
			with open(exportPath, "w") as outFile:
				json.dump(self._solver.getSolvedData(), outFile, indent=2)

			self.stopProcessing()


	def stopProcessing(self):
		self.btnStartProcessing.setText("Start Processing")
		self.enableAllFields(True)
		self._isProcessing = False

	def enableAllFields(self, enable):
		self.gbInput.setEnabled(enable)
		self.gbSaveDataTo.setEnabled(enable)
