import sys
from cx_Freeze import setup, Executable

setup(
    name = "Face Landmark Solver",
    version = "0.1",
    description = "Solver Facial Landmark and export it for blender",
    executables = [Executable("Main.py", base = "Win32GUI")])