# aniFace : Facial MoCap tool using WebCam : Proof Of Concept

Please download 68 Facial Landmarks Preditor file from : http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2

Place downloaded .dat file in 'solver' folder.

You can donload executable file and blender add-on from this link : https://drive.google.com/open?id=1OnN650lP7WwQZwYoYPMLfDCbsILICQxC 

On downloading executable, you've to place '68 Facial Landmarks Preditor file' inside 'solver' folder and then run executable.