# ********************************************************
# 1. Initialization
# ********************************************************
import bpy
###### User Interface
class InitializationPanel(bpy.types.Panel):
	"""docstring for InitializationPanel"""
	bl_label = "Initialization"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"

	def draw(self, context):
		dataFilePathRow = self.layout.column(align=True)
		dataFilePathRow.prop(context.scene,
			"dtr_mocap_data_file_path")
		referenceNumberRow = self.layout.column(align=True)
		referenceNumberRow.prop(context.scene,
			"dtr_reference_frame")
		loadButtonRow = self.layout.column(align=True)
		loadButtonRow.operator("dtr.load_mocap_data")
		# createBoneDriverRow = self.layout.column(align=True)
		# createBoneDriverRow.operator("dtr.create_bone_drivers")
		

###### Operators
class LoadMoCapData(bpy.types.Operator):
	"""docstring for LoadMoCapData"""
	bl_idname = "dtr.load_mocap_data"
	bl_label = "Load MoCap Data and Set Reference"

	def execute(self, context):
		#Read & Validate
		dataFilePath = context.scene.dtr_mocap_data_file_path
		if not dataFilePath.endswith(".json"):
			self.report ({"ERROR"},"Invalid Data File")
			return {'CANCELLED'}
		import json
		with open(dataFilePath) as dataFile:
			# context.scene.dtr_mocap_data = json.load(dataFile)
			data = json.load(dataFile)
		# if context.scene.dtr_reference_frame > len(context.scene.dtr_mocap_data):
		if context.scene.dtr_reference_frame > len(data["frame"]):
			self.report ({"ERROR"},"Invalid Reference Frame")
			return {'CANCELLED'}
		# referenceData = context.scene.dtr_mocap_data["frame"][0]
		referenceData = data["frame"][context.scene.dtr_reference_frame]

		# Create empties
		bpy.ops.object.empty_add(
			type='SPHERE',
			location=(0.0,0.0, 0.0)
			)
		bpy.context.object.name = "facePointsParent"
		scaleFactor = 0.2
		faceOrignPoint = referenceData["nose"][3]
		parentOfLandMarks = bpy.data.objects.get("facePointsParent")
		for eachKey in referenceData.keys():
			partPoints = []
			for i in range(len(referenceData[eachKey])):
				eachPoint = referenceData[eachKey][i]
				bpy.ops.object.empty_add(
					type='PLAIN_AXES',
					location=((faceOrignPoint["x"] - eachPoint["x"]) * scaleFactor, 
						(faceOrignPoint["y"] -eachPoint["y"]) * scaleFactor, 
						0.0)
					)
				currentPointName = str(eachKey+":"+str(i))
				bpy.context.object.name = currentPointName
				currentPoint = bpy.data.objects.get(currentPointName)
				partPoints.append(currentPoint)
	
			# Create Parent for each Part 
			(midX, midY, midZ) = (0.0, 0.0, 0.0)
			bpy.ops.object.empty_add(type='SINGLE_ARROW')
			bpy.context.object.name = eachKey+"Parent"
			partParent = bpy.data.objects.get(eachKey+"Parent")
			bpy.ops.object.select_all(action="DESELECT")
			#Set parent location to mid point of part points
			noOfPoints = len(partPoints)
			for i in range(noOfPoints):
				partPoints[i].select = True
				(midX, midY, midZ) = (midX + partPoints[i].location.x, midY + partPoints[i].location.y, midZ + partPoints[i].location.z)
			partParent.location = (midX / noOfPoints, midY / noOfPoints, midZ / noOfPoints)
			partParent.select = True

			#parent 
			bpy.context.scene.objects.active = partParent
			bpy.ops.object.parent_set()
			bpy.ops.object.select_all(action="DESELECT")
			partParent.select = True
			parentOfLandMarks.select = True
			bpy.context.scene.objects.active = parentOfLandMarks
			bpy.ops.object.parent_set()

		self.report ({"INFO"},"Reference Data Loaded Successfylly")
		return{'FINISHED'}
