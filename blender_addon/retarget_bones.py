# ********************************************************
# 2. Retargetting
# ********************************************************
import bpy
from .global_fun import boneDriverItemSelected
from .global_fun import selectObjectByName
###### User Interface
class RetargettingPanel(bpy.types.Panel):
	"""docstring for InitializationPanel"""
	bl_label = "Retargetting"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"

	def draw(self, context):
		createBoneDriverRow = self.layout.row(align=True)
		createBoneDriverRow.operator("dtr.create_bone_drivers")
		retargetRow = self.layout.column(align=True)
		retargetRowSplit = retargetRow.split(0.5)
		driverList = retargetRowSplit.column(align=True)
		driverList.label("Bone Drivers")
		driverList.template_list("ListItems",
			"",
			bpy.context.scene,
			"bone_drivers",
			bpy.context.scene,
			"bone_driver_index"
			)
		influencerList = retargetRowSplit.column(align=True)
		influencerList.label("Influence Data")
		# boneList.operator("dtr.create_bone_drivers")
		influencerList.template_list("ListItems",
			"",
			bpy.context.scene,
			"bone_driver_influencers",
			bpy.context.scene,
			"bone_driver_influencers_index"
		)
		retargetControlsRow = retargetRow.column(align=True)
		retargetControlsRowSplit = retargetControlsRow.split(0.5)
		bdControlsColumn = retargetControlsRowSplit.column(align=True)
		bdControlsColumn.operator("dtr.list_action", icon="ZOOMIN", text="").action = 'BD_ADD'
		bdControlsColumn.operator("dtr.list_action", icon="ZOOMOUT", text="").action = 'BD_REMOVE'
		infControlsColumn = retargetControlsRowSplit.column(align=True)
		infControlsColumn.operator("dtr.list_action", icon="ZOOMIN", text="").action = 'INF_ADD'
		infControlsColumn.operator("dtr.list_action", icon="ZOOMOUT", text="").action = 'INF_REMOVE'
		computeOffsetRow = self.layout.row(align=True)
		computeOffsetRow.operator("dtr.compute_offset_influencer")
		setupConstraintRow = self.layout.row(align=True)
		setupConstraintRow.operator("dtr.setup_constrains")




class ListItems(bpy.types.UIList):
	"""docstring for ListItems"""
	def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
		layout.label(item.name)

# ui list item actions
class Uilist_actions(bpy.types.Operator):
	bl_idname = "dtr.list_action"
	bl_label = "Actions for Retargetter List"

	action = bpy.props.EnumProperty(
		items=(
			('BD_REMOVE', "Remove Bone Driver", ""),
			('BD_ADD', "Add Bone Driver", ""),
			('INF_REMOVE', "Remove Influencer", ""),
			('INF_ADD', "Add Influencer", "")
		)
	)

	def invoke(self, context, event):

		scn = context.scene
		try:
			selectedBoneDriverItem = scn.bone_drivers[scn.bone_driver_index]
		except Exception as e:
			self.report ({"ERROR"},"Please select a BoneDriver to add Influencer")
			return {"CANCELLED"}

		if self.action == 'INF_ADD':
			#get Selected object
			selectedObjects = bpy.context.selected_objects
			print (str(scn.boneInfluenceMapper[selectedBoneDriverItem.name]["offset"])+">.......>>>>>>>>>>")
			for eachSelectedObject in selectedObjects:
				scn.boneInfluenceMapper[selectedBoneDriverItem.name]["influencers"].append(eachSelectedObject.name)
			boneDriverItemSelected(self, context)

		elif self.action == "INF_REMOVE":
			try:
				#get selected influencer
				item = scn.bone_driver_influencers[scn.bone_driver_influencers_index]
			except Exception as e:
				self.report ({"ERROR"},"Please select an Influencer to remove")
				return {"CANCELLED"}

			scn.boneInfluenceMapper[selectedBoneDriverItem.name]["influencers"].remove(item.name)
			boneDriverItemSelected(self, context)

		elif self.action == "BD_ADD":
			bpy.ops.dtr.create_bone_drivers()

		elif self.action == "BD_REMOVE":
			bpy.ops.object.select_all(action="DESELECT")
			print ("Selected Bone Driver : " + selectedBoneDriverItem.name)
			bdObject = bpy.data.objects.get(selectedBoneDriverItem.name)
			bpy.context.scene.objects.active = bdObject
			print ("Selected Bone Driver Object : " + str(bdObject))
			bdObject.select = True
			bpy.ops.object.delete(use_global=False)
			print ("Deleting From Mapper: " + selectedBoneDriverItem.name)
			if selectedBoneDriverItem.name in scn.boneInfluenceMapper :
				del scn.boneInfluenceMapper[selectedBoneDriverItem.name]
			print ("List Of Bone Drivers Before Remove : " + str(context.scene.bone_drivers))
			context.scene.bone_drivers.remove(scn.bone_driver_index)
			print ("List Of Bone Drivers After Remove : " + str(context.scene.bone_drivers))
			print ("scn.boneInfluenceMapper : " + str(scn.boneInfluenceMapper))
			boneDriverItemSelected(self, context)

		return {"FINISHED"}


# class BoneDriver(bpy.types.PropertyGroup):
# 	name = bpy.props.StringProperty()
# 	childs = []


# class Influencer(bpy.types.PropertyGroup):
# 	name = bpy.props.StringProperty()


class ListItemProperty(bpy.types.PropertyGroup):
	name = bpy.props.StringProperty()
		

class CreateBoneDrivers(bpy.types.Operator):
	bl_idname = "dtr.create_bone_drivers"
	bl_label = "Create Bone Drivers"

	def execute(self, context):
		#Get Selected Bones
		selectedPoseBones = context.selected_pose_bones
		if not selectedPoseBones:
			self.report ({"ERROR"},"Please select bone(s)")
			return{'CANCELLED'}
		armatureObject = bpy.context.object
		tempBoneLocs = {}
		for eachPoseBone in selectedPoseBones:
			boneMatrix = eachPoseBone.matrix
			boneLocation = eachPoseBone.location
			objectWorldMatrix = armatureObject.matrix_world
			globalBoneLocation = objectWorldMatrix * boneMatrix * boneLocation
			tempBoneLocs[eachPoseBone.name] = globalBoneLocation

		bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
		boneDriversParent = bpy.data.objects.get("boneDriversParent")
		if not boneDriversParent:
			bpy.ops.object.empty_add(
				type='SINGLE_ARROW',
				location = (.0, .0, .0))
			bpy.context.object.name = "boneDriversParent"
			boneDriversParent = bpy.data.objects.get("boneDriversParent")

		for boneName in tempBoneLocs.keys():
			boneDriverName = "bd_" + boneName
			bpy.ops.object.empty_add(
				type='PLAIN_AXES',
				location=tempBoneLocs[boneName])
			bpy.context.object.name = boneDriverName
			print ("::{}".format(tempBoneLocs[boneName]))
			bpy.ops.object.select_all(action="DESELECT")
			currentBoneDriver = bpy.data.objects.get(boneDriverName)
			currentBoneDriver.select = True
			boneDriversParent.select = True
			bpy.context.scene.objects.active = boneDriversParent
			bpy.ops.object.parent_set()

			boneDriverItem = context.scene.bone_drivers.add()
			boneDriverItem.name = boneDriverName
			# bpy.types.Scene.boneInfluenceMapper
			context.scene.boneInfluenceMapper[boneDriverName] = {}
			context.scene.boneInfluenceMapper[boneDriverName]["influencers"] = []
			context.scene.boneInfluenceMapper[boneDriverName]["offset"] = (.0, .0, .0)
		
		context.scene.armatureName["name"] = armatureObject.name

		return {"FINISHED"}


class ComputeInfluencerOffset(bpy.types.Operator):
	bl_idname = "dtr.compute_offset_influencer"
	bl_label = "Compute Offset from Influencers to Bone Drivers"

	def execute(self, context):
		import mathutils
		mapList = context.scene.boneInfluenceMapper
		for eachBoneDriver in mapList.keys():
			#Calculate Mid Point
			bpy.ops.object.select_all(action="DESELECT")
			noOfInfluencers = len(mapList[eachBoneDriver]["influencers"])
			midPoint = mathutils.Vector((.0,.0,.0))
			for eachInfluencer in mapList[eachBoneDriver]["influencers"]:
				# midPoint += mathutils.Vector(bpy.data.objects.get(eachInfluencer).location)
				bpy.data.objects.get(eachInfluencer).select = True

			bpy.ops.view3d.snap_cursor_to_selected()
			midPoint = bpy.context.scene.cursor_location

			#Calculate Offset
			boneDriverLocation = bpy.data.objects.get(eachBoneDriver).location
			mapList[eachBoneDriver]["offset"] = boneDriverLocation - midPoint
		return {"FINISHED"}


class SetupConstrains(bpy.types.Operator):
	bl_idname = "dtr.setup_constrains"
	bl_label = "Setup Constrains"

	def execute(self, context):
		#Get Bone Drivers
		boneDrivers = context.scene.boneInfluenceMapper.keys()
		armatureObj = selectObjectByName(context.scene.armatureName["name"])
		context.scene.objects.active = armatureObj
		bpy.ops.object.mode_set(mode="POSE", toggle=False)
		for boneDriver in boneDrivers:
			boneName = boneDriver.split("bd_")[1]
			armatureName = context.scene.armatureName["name"]
			boneObject = bpy.data.objects[armatureName].data.bones[boneName]
			bpy.ops.pose.select_all(action="DESELECT")
			boneObject.select = True
			bpy.data.objects[armatureName].data.bones.active = boneObject
			bpy.ops.pose.constraint_add(type='CHILD_OF')
			# print ("{} - {} - {}".format(boneName, armatureName, boneDriver))
			# print ("bpy.context.object.pose.bones[{}].constraints[\"Child Of\"].target = bpy.data.objects[{}]".format(boneName, boneDriver))
			# Refer : https://developer.blender.org/T39891 : Why to override constraint
			bpy.context.object.pose.bones[boneName].constraints["Child Of"].target = bpy.data.objects[boneDriver]
			myContext = bpy.context.copy()
			myContext["constraint"] = bpy.context.object.pose.bones[boneName].constraints["Child Of"]
			bpy.ops.constraint.childof_set_inverse(myContext, constraint="Child Of", owner='BONE')
		return {"FINISHED"}

