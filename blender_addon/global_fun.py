# ********************************************************
# Globals & Functions
# ********************************************************
import bpy

def boneDriverItemSelected(self, context):
	selectedBoneDriverItem = context.scene.bone_drivers[context.scene.bone_driver_index]
	print ("Selected {}".format(selectedBoneDriverItem.name))
	selectObjectByName(selectedBoneDriverItem.name)
	influencerList = context.scene.bone_driver_influencers
	# selectedBoneDriverItem.childs.append(selectedBoneDriverItem.name + "_DTR")
	boneInfluenceMapper = context.scene.boneInfluenceMapper
	print("############")
	print(selectedBoneDriverItem.name)
	print(boneInfluenceMapper[selectedBoneDriverItem.name])
	print("############")
	if len(influencerList) > 0:
		#reverse range to remove last item first
		for i in range(len(influencerList)-1,-1,-1):
			influencerList.remove(i)
	for child in boneInfluenceMapper[selectedBoneDriverItem.name]["influencers"]:
		#Add New items
		item = influencerList.add()
		item.name = child

def boneInfluencerItemSelected(self, context):
	scn = context.scene
	selectedInfluencerItem = scn.bone_driver_influencers[scn.bone_driver_influencers_index]
	selectObjectByName(selectedInfluencerItem.name)

def selectObjectByName(name):
	obj = bpy.data.objects.get(name)
	bpy.ops.object.select_all(action="DESELECT")
	obj.select = True
	return obj