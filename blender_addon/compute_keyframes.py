# ********************************************************
# 3. Keyframing
# ********************************************************
import bpy

class KeyframingPanel(bpy.types.Panel):
	"""docstring for KeyframingPanel"""
	bl_label = "Retargetting"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	
	def draw(self, context):
		column = self.layout.column(align=True)
		column.prop(context.scene, "dtr_start_keyframing_from")
		column.prop(context.scene, "dtr_set_timeline")
		column.operator("dtr.compute_keyframes")


class ComputeKeyframes(bpy.types.Operator):
	"""docstring for ComputeKeyframes"""
	bl_idname = "dtr.compute_keyframes"
	bl_label = "Compute Offset from Influencers to Bone Drivers"

	def execute(self, context):
		startFrom = context.scene.dtr_start_keyframing_from
		setTimeline = context.scene.dtr_set_timeline
		bpy.context.scene.frame_set(startFrom)

		#Read & Validate
		dataFilePath = context.scene.dtr_mocap_data_file_path
		if not dataFilePath.endswith(".json"):
			self.report ({"ERROR"},"Invalid Data File")
			return {'CANCELLED'}
		import json
		with open(dataFilePath) as dataFile:
			# context.scene.dtr_mocap_data = json.load(dataFile)
			data = json.load(dataFile)

		if setTimeline is True :
			context.scene.frame_start = startFrom
			context.scene.frame_end = startFrom + len(data["frame"]) 
		scaleFactor = 0.2
		import mathutils
		
		for frameIndex in range(len(data["frame"])):
			bpy.context.scene.frame_set(startFrom + frameIndex)
			eachFrameData = data["frame"][frameIndex]
			faceOrignPoint = eachFrameData["nose"][3]
			for eachBoneDriver in context.scene.bone_drivers:
				bpy.ops.object.select_all(action="DESELECT")
				influencers = context.scene.boneInfluenceMapper[eachBoneDriver.name]["influencers"]
				offset = context.scene.boneInfluenceMapper[eachBoneDriver.name]["offset"]
				for eachInfluencer in influencers:
					infObject = bpy.data.objects.get(eachInfluencer)
					infNameSplit = eachInfluencer.split(':')
					infLocData = eachFrameData[infNameSplit[0]][int(infNameSplit[1])]
					infObject.location = (
						(faceOrignPoint["x"] - infLocData["x"]) * scaleFactor, 
						(faceOrignPoint["y"] - infLocData["y"]) * scaleFactor, 
						0.0
					)
					infObject.select = True
				bpy.ops.view3d.snap_cursor_to_selected()
				midPoint = bpy.context.scene.cursor_location
				boneDriverObj = bpy.data.objects.get(eachBoneDriver.name)
				newLoc = mathutils.Vector(midPoint) + mathutils.Vector(offset)
				boneDriverObj.location = newLoc
				bpy.ops.object.select_all(action="DESELECT")
				boneDriverObj.select = True
				bpy.ops.anim.keyframe_insert_menu(type='Location')


		return {"FINISHED"}
		