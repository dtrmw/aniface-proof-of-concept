bl_info = {
    "name": "aniFace",
    "author": "Dharma Teja Reddy K",
    "version": (0, 1, 0),
    "location": "View3D > Tools > aniFace",
    "description": "Proof Of Concept tool for Facial MoCap using webcam.",
    "category": "Tracking"
}

import bpy
from .load_reference import *
from .retarget_bones import *
from .compute_keyframes import *
from .global_fun import *

# ********************************************************
# Registration
# ********************************************************
def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.dtr_mocap_data_file_path = bpy.props.StringProperty \
    (
        name = "MoCap Data File",
        description = "Browse/Paste MoCap Data File",
        default = "",
        subtype='FILE_PATH'
    )
    bpy.types.Scene.dtr_reference_frame = bpy.props.IntProperty \
    (
        name = "Reference Frame:",
        description = "Specify Reference Frame Number",
        default = 1,
        step=1,
        min=1
    )
    bpy.types.Scene.dtr_mocap_data = {}
    bpy.types.Scene.bone_drivers = bpy.props.CollectionProperty(type=ListItemProperty)
    bpy.types.Scene.bone_driver_index = bpy.props.IntProperty(update=boneDriverItemSelected)
    bpy.types.Scene.bone_driver_influencers = bpy.props.CollectionProperty(type=ListItemProperty)
    bpy.types.Scene.bone_driver_influencers_index = bpy.props.IntProperty(update=boneInfluencerItemSelected)
    bpy.types.Scene.boneInfluenceMapper = {}
    bpy.types.Scene.armatureName = {"name":None}
    bpy.types.Scene.dtr_start_keyframing_from = bpy.props.IntProperty \
    (
        name = "Start Keyframing From:",
        description = "Specify from where to start Keyframing",
        default = 101,
        step=1,
        min=1
    )
    bpy.types.Scene.dtr_set_timeline = bpy.props.BoolProperty(
            name = "Set Timeline Length"
        )

def unregister() :
    del bpy.types.Scene.dtr_mocap_data_file_path
    del bpy.types.Scene.dtr_reference_frame
    del bpy.types.Scene.dtr_mocap_data
    del bpy.types.Scene.bone_drivers
    del bpy.types.Scene.bone_driver_index
    del bpy.types.Scene.bone_driver_influencers
    del bpy.types.Scene.bone_driver_influencers_index
    del bpy.types.Scene.boneInfluenceMapper
    del bpy.types.Scene.armatureName


if __name__ == '__main__':
    register()
