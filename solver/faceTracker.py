import cv2
import dlib
import imutils
import numpy as np
from imutils import face_utils

class FaceLandmarkSolver(object):
	"""docstring for FaceLandmarkSolver"""
	def __init__(self, 
		videoPath, 
		predictorFile
		):

		super(FaceLandmarkSolver, self).__init__()
		if not predictorFile.endswith(".dat"):
			raise Exception("Invalid Predictor File given to solver")
		self._predictorFile = predictorFile
		self._videoPath = videoPath
		# self._data = {
		# 	"mouth":[],
		# 	"right_eyebrow":[],
		# 	"left_eyebrow":[],
		# 	"right_eye":[],
		# 	"left_eye":[],
		# 	"nose":[],
		# 	"jaw":[]
		# }
		self._data = {
			"frame" : []
		}

	def startSolving(self):
		detector = dlib.get_frontal_face_detector()
		predictor = dlib.shape_predictor(self._predictorFile)
		cap = cv2.VideoCapture(self._videoPath)
		
		while cap.isOpened():
			ret, image = cap.read()
			if not ret:
				break
			try:
				image = imutils.resize(image, width=720)
			except Exception as e:
				print("Can not resize this image. Stopping")
				break
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

			frameData = {}
			#detect faces in gray scale image
			rects = detector(gray, 1)
			for (i, rect) in enumerate(rects):
				shape = predictor(gray, rect)
				shape = face_utils.shape_to_np(shape)
				clone = image.copy()
				for (name, (i,j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
					frameData[name] = []
					for (x,y) in shape[i:j]:
						frameData[name].append({"x":int(x),"y":int(y)})
						cv2.circle(clone, (x, y), 1, (0, 255, 0), -1)

				cv2.imshow("Processed Image", clone)
			self._data["frame"].append(frameData)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		cap.release()
		cv2.destroyAllWindows()
		return 0

	def getSolvedData(self):
		return self._data
