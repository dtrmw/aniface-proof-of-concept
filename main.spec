# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['C:\\Users\\Dharma Teja Reddy K\\Documents\\DTRMediaWorks\\aniFace'],
             binaries=[('C:\\Program Files\\Python36\\Lib\\site-packages\\cv2\\opencv_ffmpeg330_64.dll', '.')],
             datas=[('solver/shape_predictor_68_face_landmarks.dat', './solver/shape_predictor_68_face_landmarks.dat')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='main',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
